package example.Squizz.Controller;


import example.Squizz.Model.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController {
    @MessageMapping("/user")
    @SendTo("/topic/chat")
    public ChatMessage send(@Payload ChatMessage message) {
        return message;
    }
}
