package example.Squizz.Controller;


import example.Squizz.Interface.PersonRepository;
import example.Squizz.Interface.QuizRepository;
import example.Squizz.Model.Choices;
import example.Squizz.Model.Person;
import example.Squizz.Model.Question;
import example.Squizz.Model.Quiz;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller  // This means that this class is a Controller
@RequestMapping("/quiz") // This means URL's start with /api (after Application path)
@CrossOrigin(origins = "http://localhost:3000")
public class QuizController {

    @Autowired
    private QuizRepository quizRepository;

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Quiz> getAllQuizzes() {
        // This returns a JSON or XML with the users
        return quizRepository.findAll();
    }

    @PostMapping("/create")
    public @ResponseBody String createQuiz(@RequestParam String questions, @RequestParam String answers, HttpServletResponse response){
        HashMap<String, Object> data = new HashMap<>();

        HashMap<String, Object> question = new HashMap<>();
        HashMap<String, Object> answer = new HashMap<>();

        List<Question> questionList = new ArrayList<>();

        answer.put("answer", answers);
        question.put(questions, answer);
        System.out.println(question.toString());


        try{
            response.setStatus(HttpServletResponse.SC_OK);
            data.put("Quiz", question);
            data.put("message", "Quiz generated successfully");
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            data.put("message", "Quiz generated unsuccessfully");
        }

        return new JSONObject(data).toString();
    }
}
