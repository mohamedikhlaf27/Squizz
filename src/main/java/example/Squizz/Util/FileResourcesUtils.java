package example.Squizz.Util;

import com.microsoft.sqlserver.jdbc.StringUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.Scanner;
import java.util.stream.Collectors;


public class FileResourcesUtils  {
//    public static void main(String[] args) throws IOException {
//
//        FileResourcesUtils app = new FileResourcesUtils();
//
//        //String fileName = "database.properties";
//        String fileName = "json/file1.json";
//
//        System.out.println("getResourceAsStream : " + fileName);
//        InputStream is = app.getFileFromResourceAsStream(fileName);
//        System.out.println(is);
//    }

    // get a file from the resources folder
    // works everywhere, IDEA, unit test and JAR file.
    public InputStream getFileFromResourceAsStream(String fileName) {

        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        // the stream holding the file content
        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }
    }

    public File loadEmployeesWithSpringInternalClass()
            throws FileNotFoundException {
        return ResourceUtils.getFile(
                "src/main/resources/jwtkey.json");
    }
}