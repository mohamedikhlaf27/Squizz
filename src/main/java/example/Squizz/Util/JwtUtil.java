package example.Squizz.Util;

import example.Squizz.Model.Person;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {

    private String SECRET_KEY;

    public JwtUtil() throws Exception {
        //String currentPath = new File(JwtUtil.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
        String runtimePath = new File(".").getCanonicalPath();

        System.out.println("current path" + runtimePath );

        //String jsonKey = readFileAsString(".\\BOOT-INF\\classes\\jwtkey.json");
        //String jsonKey = readFileAsString("..\\..\\..\\jwtkey.json");
        //String file = readFileAsString("src/main/resources/jwtkey.json");
        //System.out.println("File: " + file);
        //String jsonKey = getFileFromResourceAsStream(file).toString();
        //System.out.println("jsonkey: " + jsonKey);
        FileResourcesUtils fileResourcesUtils = new FileResourcesUtils();
        File jsonKey = new File(readFileAsString(fileResourcesUtils.loadEmployeesWithSpringInternalClass()));

        JSONObject obj = new JSONObject(jsonKey.toString());
        this.SECRET_KEY = obj.getString("key");
    }

    public static String readFileAsString(File file) throws Exception
    {
        return new String(Files.readAllBytes(Paths.get(String.valueOf(file))));
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(Person userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getEmail());
    }

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, Person userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getEmail()) && !isTokenExpired(token));
    }

    public InputStream getFileFromResourceAsStream(String fileName) {

        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);
        System.out.println("-------------------------------------------" + inputStream);

        // the stream holding the file content
        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }
    }
}