package example.Squizz.Model;

import lombok.Data;

public @Data class ChatMessage {
    private String name;
    private String message;
}
