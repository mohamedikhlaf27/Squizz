package example.Squizz;

import static org.assertj.core.api.Assertions.assertThat;

import example.Squizz.Controller.QuizController;
import example.Squizz.Controller.UserController;
import example.Squizz.Util.Authorizer;
import example.Squizz.Util.Util;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private UserController userController;

    @Autowired
    private QuizController quizController;

    @Mock
    private Util util;
    @Mock
    private Authorizer authorizer;

    @Test
    public void contextLoads() {
        assertThat(userController).isNotNull();
        assertThat(quizController).isNotNull();
        assertThat(util).isNotNull();
        assertThat(authorizer).isNotNull();
    }
}
