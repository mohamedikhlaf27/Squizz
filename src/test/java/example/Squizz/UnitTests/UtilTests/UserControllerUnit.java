package example.Squizz.UnitTests.UtilTests;

import example.Squizz.Controller.UserController;
import example.Squizz.Util.Util;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@SpringBootTest
public class UserControllerUnit {

    @Test
    public void UnauthorizedAccessTest(){
        // Given
        String message = "Email wrong";
        HttpServletResponse response = new MockHttpServletResponse();
        HashMap<String, Object> data = new HashMap<>();
        UserController userController = new UserController();

        // When
        userController.UnauthorizedAccess(response, data, message);

        //Assert
        Assert.isTrue(!data.isEmpty(),  "data not empty");
    }
}
