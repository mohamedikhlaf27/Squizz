FROM openjdk:15-jdk-alpine
MAINTAINER Mohamed
WORKDIR /Squizz
COPY /target/Squizz-1.jar Squizz-1.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "Squizz-1.jar"]
